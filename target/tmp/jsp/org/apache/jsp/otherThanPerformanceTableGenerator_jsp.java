package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import domain.*;
import managers.*;

public final class otherThanPerformanceTableGenerator_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<title>Tabelka</title>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      domain.CalculatorParameters parameters = null;
      synchronized (session) {
        parameters = (domain.CalculatorParameters) _jspx_page_context.getAttribute("parameters", PageContext.SESSION_SCOPE);
        if (parameters == null){
          parameters = new domain.CalculatorParameters();
          _jspx_page_context.setAttribute("parameters", parameters, PageContext.SESSION_SCOPE);
        }
      }
      out.write('\r');
      out.write('\n');
      domain.CalculatorApplication calculator = null;
      synchronized (session) {
        calculator = (domain.CalculatorApplication) _jspx_page_context.getAttribute("calculator", PageContext.SESSION_SCOPE);
        if (calculator == null){
          calculator = new domain.CalculatorApplication();
          _jspx_page_context.setAttribute("calculator", calculator, PageContext.SESSION_SCOPE);
        }
      }
      out.write('\r');
      out.write('\n');
      managers.Calculator calculatorService = null;
      synchronized (application) {
        calculatorService = (managers.Calculator) _jspx_page_context.getAttribute("calculatorService", PageContext.APPLICATION_SCOPE);
        if (calculatorService == null){
          calculatorService = new managers.Calculator();
          _jspx_page_context.setAttribute("calculatorService", calculatorService, PageContext.APPLICATION_SCOPE);
        }
      }
      out.write("\r\n");
      out.write("\r\n");
      org.apache.jasper.runtime.JspRuntimeLibrary.introspecthelper(_jspx_page_context.findAttribute("parameters"), "type", request.getParameter("type"), request, "type", false);
      out.write('\r');
      out.write('\n');
      org.apache.jasper.runtime.JspRuntimeLibrary.introspecthelper(_jspx_page_context.findAttribute("parameters"), "year", request.getParameter("year"), request, "year", false);
      out.write('\r');
      out.write('\n');
      org.apache.jasper.runtime.JspRuntimeLibrary.introspecthelper(_jspx_page_context.findAttribute("parameters"), "salary", request.getParameter("salary"), request, "salary", false);
      out.write('\r');
      out.write('\n');
      org.apache.jasper.runtime.JspRuntimeLibrary.introspecthelper(_jspx_page_context.findAttribute("parameters"), "netOrGross", request.getParameter("netOrGross"), request, "netOrGross", false);
      out.write('\r');
      out.write('\n');
      org.apache.jasper.runtime.JspRuntimeLibrary.introspecthelper(_jspx_page_context.findAttribute("parameters"), "incomeCost", request.getParameter("incomeCost"), request, "incomeCost", false);
      out.write("\r\n");
      out.write("\r\n");
 String s = "gross";
String p = parameters.getNetOrGross(); 
double salary = parameters.getSalary();
double skladkaChorobowa = 0;
double skladkaEmerytalna = 0;
double skladkaRentowa = 0;
double skladkaZdrowotna = 0;
double podstawa = 0;
double zaliczka = 0;

      out.write("\r\n");
      out.write("\r\n");
if (s.equals(p)){ 
      out.write('\r');
      out.write('\n');
parameters.setSalaryBrutto(salary);
skladkaChorobowa = parameters.getSalaryBrutto()*(2.45/100);
skladkaEmerytalna = parameters.getSalaryBrutto()*(9.76/100);
skladkaRentowa = parameters.getSalaryBrutto()*(1.5/100);
skladkaZdrowotna = parameters.getSalaryBrutto()*(9.0/100);
podstawa = parameters.getSalaryBrutto()-skladkaChorobowa-skladkaEmerytalna-skladkaRentowa-skladkaZdrowotna;
zaliczka = (parameters.getSalaryBrutto()-podstawa)*0.22;
parameters.setSalaryNetto(podstawa-zaliczka);
      out.write('\r');
      out.write('\n');
} else { 
      out.write('\r');
      out.write('\n');

parameters.setSalaryNetto(salary);
parameters.setSalaryBrutto(parameters.getSalaryNetto()*1.39);
skladkaChorobowa = parameters.getSalaryBrutto()*(2.45/100);
skladkaEmerytalna = parameters.getSalaryBrutto()*(9.76/100);
skladkaRentowa = parameters.getSalaryBrutto()*(1.5/100);
skladkaZdrowotna = parameters.getSalaryBrutto()*(9.0/100);
podstawa = parameters.getSalaryBrutto()-skladkaChorobowa-skladkaEmerytalna-skladkaRentowa-skladkaZdrowotna;
zaliczka = (parameters.getSalaryBrutto()-podstawa)*0.22;


      out.write("\r\n");
      out.write("\r\n");
} 
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<h1>Oto tabelka wykonana specjalnie dla Ciebie:</h1></br>\r\n");
      out.write("\r\n");
      out.write("<table border='1'>\r\n");
      out.write("<tr><td rowspan=\"2\">Miasiac nr</td><td rowspan=\"2\">Brutto</td><td colspan =\"4\">Ubezpieczenie</td><td rowspan=\"2\">Podstawa opodatkowania</td><td rowspan=\"2\">Zaliczka na PIT</td><td rowspan=\"2\">Netto</td></tr>\r\n");
      out.write("<td>emerytalne</td><td>rentowe</td><td>chorobowe</td><td>zdrowotne</td></tr>\r\n");
for(int i = 1; i < 13; i++ ){ 
      out.write("\r\n");
      out.write("<tr><td>");
      out.print(i);
      out.write("</td>\r\n");
      out.write("<td>");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((domain.CalculatorParameters)_jspx_page_context.findAttribute("parameters")).getSalaryBrutto())));
      out.write("</td>\r\n");
      out.write("<td>");
      out.print( Math.round(skladkaEmerytalna));
      out.write("</td>\r\n");
      out.write("<td>");
      out.print( Math.round(skladkaRentowa));
      out.write("</td>\r\n");
      out.write("<td>");
      out.print( Math.round(skladkaChorobowa));
      out.write("</td>\r\n");
      out.write("<td>");
      out.print( Math.round(skladkaZdrowotna));
      out.write("</td>\r\n");
      out.write("<td>");
      out.print( Math.round(podstawa));
      out.write("</td>\r\n");
      out.write("<td>");
      out.print( Math.round(zaliczka));
      out.write("</td>\r\n");
      out.write("<td>");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((domain.CalculatorParameters)_jspx_page_context.findAttribute("parameters")).getSalaryNetto())));
      out.write("</td>\r\n");
      out.write("</tr>\r\n");
} 
      out.write("\r\n");
      out.write("\r\n");
      out.write("<tr><td>suma</td>\r\n");
      out.write("<td>");
      out.print( parameters.getSalaryBrutto()*12 );
      out.write("\r\n");
      out.write("<td>");
      out.print( Math.round(skladkaEmerytalna)*12);
      out.write("</td>\r\n");
      out.write("<td>");
      out.print( Math.round(skladkaRentowa)*12);
      out.write("</td>\r\n");
      out.write("<td>");
      out.print( Math.round(skladkaChorobowa)*12);
      out.write("</td>\r\n");
      out.write("<td>");
      out.print( Math.round(skladkaZdrowotna)*12);
      out.write("</td>\r\n");
      out.write("<td>");
      out.print( Math.round(podstawa)*12);
      out.write("</td>\r\n");
      out.write("<td>");
      out.print( Math.round(zaliczka)*12);
      out.write("</td>\r\n");
      out.write("<td>");
      out.print( parameters.getSalaryNetto()*12 );
      out.write("\r\n");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
