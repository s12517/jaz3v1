package domain;

public class CalculatorParameters {

	private int skladkaRentowa;
	private int skladkaEmerytalna;
	private int skladkaChorobowa;
	private int skladkaZdrowotna;
	private float incomeCost;
	private double incomeAmount;
	private String type;
	private int year;
	private double salaryNetto;
	private double salaryBrutto;
	private double salary;
	private String netOrGross;

	
	public int getSkladkaRentowa() {
		return skladkaRentowa;
	}

	public void setSkladkaRentowa(int skladkaRentowa) {
		this.skladkaRentowa = skladkaRentowa;
	}


	public int getSkladkaEmerytalna() {
		return skladkaEmerytalna;
	}

	public void setSkladkaEmerytalna(int skladkaEmerytalna) {
		this.skladkaEmerytalna = skladkaEmerytalna;
	}

	public int getSkladkaChorobowa() {
		return skladkaChorobowa;
	}

	public void setSkladkaChorobowa(int skladkaChorobowa) {
		this.skladkaChorobowa = skladkaChorobowa;
	}

	public int getSkladkaZdrowotna() {
		return skladkaZdrowotna;
	}

	public void setSkladkaZdrowotna(int skladkaZdrowotna) {
		this.skladkaZdrowotna = skladkaZdrowotna;
	}

	public float getIncomeCost() {
		return incomeCost;
	}

	public void setIncomeCost(float incomeCost) {
		this.incomeCost = incomeCost;
	}

	public double getIncomeAmount() {
		return incomeAmount;
	}

	public void setIncomeAmount(double incomeAmount) {
		this.incomeAmount = incomeAmount;
	}

	public String getNetOrGross() {
		return netOrGross;
	}

	public void setNetOrGross(String netOrGross) {
		this.netOrGross = netOrGross;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public double getSalaryNetto() {
		return salaryNetto;
	}

	public void setSalaryNetto(double salaryNetto) {
		this.salaryNetto = salaryNetto;
	}

	public double getSalaryBrutto() {
		return salaryBrutto;
	}

	public void setSalaryBrutto(double salaryBrutto) {
		this.salaryBrutto = salaryBrutto;
	}
	
	
	
}

