<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "domain.CalculatorParameters" %>
<%@ page import = "javax.servlet.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Formularz danych szczegolowych</title>
</head>
<body>

<jsp:useBean id="parameters" class="domain.CalculatorParameters" scope="session"/>
<jsp:useBean id="calculator" class="domain.CalculatorApplication" scope="session"/>

Wybrales umowe: <jsp:getProperty name="parameters" property="type"/> <br/>
Wybrales rok: <jsp:getProperty name="parameters" property="year"/></br>
Wybrales kwote: <jsp:getProperty name="parameters" property="salary"/></br>
Wybrales typ kwoty: <jsp:getProperty name="parameters" property="netOrGross"/></br></br>

Podaj jeszcze kilka szczegolow:</br>
<form action="orderTableGenerator.jsp">
<label>1. Jaki jest Twoj koszt przychodu:</label></br>
<label>50%<input type="radio" value="50" name="incomeCost" id="incomeCost"/></label></br>
<label>20%<input type="radio" value="20" name="incomeCost" id="incomeCost"/></label></br></br>

<label>2. Czy odprowadzasz nastepujace skladki:</label></br>
<label>a) skladka rentowa:</label></br>
<label>tak<input type="radio" value="1" name="skladkaRentowa" id="skladkaRentowa"/></label></br>
<label>nie<input type="radio" value="0" name="skladkaRentowa" id="skladkaRentowa"/></label></br>
<label>b) skladka emerytalna:</label></br>
<label>tak<input type="radio" value="1" name="skladkaEmerytalna" id="skladkaEmerytalna"/></label></br>
<label>nie<input type="radio" value="0" name="skladkaEmerytalna" id="skladkaEmerytalna"/></label></br>
<label>c) skladka chorobowa:</label></br>
<label>tak<input type="radio" value="1" name="skladkaChorobowa" id="skladkaChorobowa"/></label></br>
<label>nie<input type="radio" value="0" name="skladkaChorobowa" id="skladkaChorobowa"/></label></br>
<label>d) skladka zdrowotna:</label></br>
<label>tak<input type="radio" value="1" name="skladkaZdrowotna" id="skladkaZdrowotna"/></label></br>
<label>nie<input type="radio" value="0" name="skladkaZdrowotna" id="skladkaZdrowotna"/></label></br>

<input type = "submit" value="zobacz rezultat"/>
</form>

</body>
</html>