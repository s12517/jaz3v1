<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding ="ISO-8859-1"%>
<%@ page import = "domain.*" %>
<%@ page import = "managers.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Tabelka</title>
</head>
<body>


<jsp:useBean id="parameters" class="domain.CalculatorParameters" scope="session"/>
<jsp:useBean id="calculator" class="domain.CalculatorApplication" scope="session"/>
<jsp:useBean id="calculatorService" class="managers.Calculator" scope="application"/>

<jsp:setProperty name="parameters" property="type" param ="type"/>
<jsp:setProperty name="parameters" property="year" param ="year"/>
<jsp:setProperty name="parameters" property="salary" param ="salary"/>
<jsp:setProperty name="parameters" property="netOrGross" param ="netOrGross"/>
<jsp:setProperty name="parameters" property="incomeCost" param ="incomeCost"/>
<jsp:setProperty name="parameters" property="skladkaRentowa" param ="skladkaRentowa"/>
<jsp:setProperty name="parameters" property="skladkaEmerytalna" param ="skladkaEmerytalna"/>
<jsp:setProperty name="parameters" property="skladkaChorobowa" param ="skladkaChorobowa"/>
<jsp:setProperty name="parameters" property="skladkaZdrowotna" param ="skladkaZdrowotna"/>

<% String s = "gross";
String p = parameters.getNetOrGross(); 
double salary = parameters.getSalary();
double skladkaChorobowa = 0;
double skladkaEmerytalna = 0;
double skladkaRentowa = 0;
double skladkaZdrowotna = 0;
double podstawa = 0;
double zaliczka = 0;
double koszt = 0;
%>

<%if (s.equals(p)){ %>
<%parameters.setSalaryBrutto(salary);
koszt = parameters.getSalaryBrutto()*(parameters.getIncomeCost()/100);
skladkaChorobowa = parameters.getSkladkaChorobowa()*parameters.getSalaryBrutto()*(2.45/100);
skladkaEmerytalna = parameters.getSkladkaEmerytalna()*parameters.getSalaryBrutto()*(9.76/100);
skladkaRentowa = parameters.getSkladkaRentowa()*parameters.getSalaryBrutto()*(1.5/100);
skladkaZdrowotna = parameters.getSkladkaZdrowotna()*parameters.getSalaryBrutto()*(9.0/100);
podstawa = parameters.getSalaryBrutto()-skladkaChorobowa-skladkaEmerytalna-skladkaRentowa-skladkaZdrowotna;
zaliczka = (parameters.getSalaryBrutto()-podstawa)*0.22;
parameters.setSalaryNetto(podstawa-zaliczka);%>
<%} else { %>
<%
parameters.setSalaryNetto(salary);
koszt = parameters.getSalaryNetto()*0.24;
parameters.setSalaryBrutto(parameters.getSalaryNetto()*1.39);
skladkaChorobowa = parameters.getSalaryBrutto()*(2.45/100);
skladkaEmerytalna = parameters.getSalaryBrutto()*(9.76/100);
skladkaRentowa = parameters.getSalaryBrutto()*(1.5/100);
skladkaZdrowotna = parameters.getSalaryBrutto()*(9.0/100);
podstawa = parameters.getSalaryBrutto()-skladkaChorobowa-skladkaEmerytalna-skladkaRentowa-skladkaZdrowotna;
zaliczka = (parameters.getSalaryBrutto()-podstawa)*0.22;

%>

<%} %>


<h1>Oto tabelka wykonana specjalnie dla Ciebie:</h1></br>

<table border='1'>
<tr><td rowspan="2">Brutto</td><td colspan ="4">Ubezpieczenie</td><td rowspan="2">Koszt uzyskania przychodu</td><td rowspan="2">Podstawa opodatkowania</td><td rowspan="2">Zaliczka na PIT</td><td rowspan="2">Netto</td></tr>
<td>emerytalne</td><td>rentowe</td><td>chorobowe</td><td>zdrowotne</td></tr>

<td><jsp:getProperty name="parameters" property="salaryBrutto"/></td>
<td><%= Math.round(skladkaEmerytalna)%></td>
<td><%= Math.round(skladkaRentowa)%></td>
<td><%= Math.round(skladkaChorobowa)%></td>
<td><%= Math.round(skladkaZdrowotna)%></td>
<td><%= Math.round(koszt)%></td>
<td><%= Math.round(podstawa)%></td>
<td><%= Math.round(zaliczka)%></td>
<td><jsp:getProperty name="parameters" property="salaryNetto"/></td>
</tr>
</table>

</body>
</html>