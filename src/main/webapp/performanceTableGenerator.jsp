<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding ="ISO-8859-1"%>
<%@ page import = "domain.*" %>
<%@ page import = "managers.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Tabelka</title>
</head>
<body>


<jsp:useBean id="parameters" class="domain.CalculatorParameters" scope="session"/>
<jsp:useBean id="calculator" class="domain.CalculatorApplication" scope="session"/>
<jsp:useBean id="calculatorService" class="managers.Calculator" scope="application"/>

<jsp:setProperty name="parameters" property="type" param ="type"/>
<jsp:setProperty name="parameters" property="year" param ="year"/>
<jsp:setProperty name="parameters" property="salary" param ="salary"/>
<jsp:setProperty name="parameters" property="netOrGross" param ="netOrGross"/>
<jsp:setProperty name="parameters" property="incomeCost" param ="incomeCost"/>

<% String s = "gross";
String p = parameters.getNetOrGross(); 
double salary = parameters.getSalary();
double koszt = 0;
double podstawa = 0;
double zaliczka = 0;
%>

<%if (s.equals(p)){ %>
<%parameters.setSalaryBrutto(salary);
koszt = parameters.getSalaryBrutto()*(parameters.getIncomeCost()/100);
podstawa = parameters.getSalaryBrutto()-parameters.getIncomeCost();
zaliczka = (parameters.getSalaryBrutto()-parameters.getIncomeCost())*0.18;
parameters.setSalaryNetto(parameters.getSalaryBrutto()-zaliczka);%>
<%} else { %>
<%parameters.setSalaryNetto(salary);
koszt = parameters.getSalaryNetto()*0.24;
parameters.setSalaryBrutto(koszt*((100-parameters.getIncomeCost())/parameters.getIncomeCost()));
podstawa = parameters.getSalaryBrutto()-parameters.getIncomeCost();
zaliczka = (parameters.getSalaryBrutto()-parameters.getIncomeCost())*0.18;
%>
<%} %>


<h1>Oto tabelka wykonana specjalnie dla Ciebie:</h1></br>

<table border='1'>
<tr><td>Brutto</td><td>Koszt uzyskania przychodu</td><td>Podstawa opodatkowania</td><td>Zaliczka na PIT</td><td>Netto</td></tr>
<tr><td><jsp:getProperty name="parameters" property="salaryBrutto"/></td>
<td><%= Math.round(koszt) %></td>
<td><%= Math.round(podstawa)%></td>
<td><%= Math.round(zaliczka)%></td>
<td><jsp:getProperty name="parameters" property="salaryNetto"/></td>

</table>

</body>
</html>