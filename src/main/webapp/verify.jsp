<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "domain.*" %>
<%@ page import = "javax.servlet.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Formularz danych podstawowych</title>
</head>
<body>

<jsp:useBean id="parameters" class="domain.CalculatorParameters" scope="session"/>
<jsp:useBean id="calculator" class="domain.CalculatorApplication" scope="session"/>
<jsp:useBean id="calculatorService" class="managers.Calculator" scope="application"/>


<jsp:setProperty name="parameters" property="type" param ="type"/>
<jsp:setProperty name="parameters" property="year" param ="year"/>
<jsp:setProperty name="parameters" property="salary" param ="salary"/>
<jsp:setProperty name="parameters" property="netOrGross" param ="netOrGross"/>


Wybrales umowe: <jsp:getProperty name="parameters" property="type"/></br>
Wybrales rok: <jsp:getProperty name="parameters" property="year"/></br>
Wybrales kwote: <jsp:getProperty name="parameters" property="salary"/></br>
Wybrales typ kwoty: <jsp:getProperty name="parameters" property="netOrGross"/></br>

<% String p = "performance";
String t = parameters.getType(); 
String o = "order";
String e = "employment";
%>

<%if(p.equals(t)){ %>
<% response.sendRedirect("performanceForm.jsp");%>
<%}else if(o.equals(t)){ %>
<%response.sendRedirect("orderForm.jsp"); %>
<%}else if(e.equals(t)){ %>
<% response.sendRedirect("employmentTableGenerator.jsp"); %>
<%} %>

</body>
</html>